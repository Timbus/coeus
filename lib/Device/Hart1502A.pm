use 5.010;

package Device::Hart1502A;
use Mouse;
extends 'Device';
with 'Interface::COM';

#Override the default COM settings.
sub _build_stty_params {
	return [qw/
		2400 cs8 -parenb -cstopb cread raw 
		-opost -olcuc -ocrnl -onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
		-isig -icanon -iexten -echo -echoe -echok -echonl -noflsh -xcase -tostop -echoprt
		-echoctl -echoke
	/];
}

#Override *_delim to set the linebreak of the device.
sub _build_write_delim { "\r"   }
sub _build_read_delim  { "\n\n" }

my %unitconv = (
	'O' => 'Ohms',
	'C' => 'DegC',
	'F' => 'DegF',
	'K' => 'DegK',
);

sub start_read {
	my $self = shift;
	$self->write("SA=3");
	
	my @handler; @handler = ('line' => $self->read_delim,  sub {
		my ($fh, $line) = @_;
		if ($line =~ /^[tT]:\s+(?<value>[\d\.]+)\s+(?<units>[OCFK])/){
			$self->handle->push_read(@handler);
			#return ($+{value}, $unitconv{$+{units}});
		}
		else {
			die "Failed to parse temperature! Value was: $line";
		}
	});
	$self->handle->push_read(@handler);
}

sub getval {
	my $self = shift;
	
}




__PACKAGE__->meta->make_immutable;
