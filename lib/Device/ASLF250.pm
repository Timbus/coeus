use 5.010;

package Device::ASLF250;
use Mouse;
use Coro;
use AnyEvent::Handle;

extends 'Device';
with 'Interface::COM';


#Override the default COM settings.
sub _build_stty_params {
	#Ugh.
	return [qw/
		19200 cs8 -parenb cstopb cread raw
		-opost -olcuc -ocrnl -onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
		-isig -icanon -iexten -echo -echoe -echok -echonl -noflsh -xcase -tostop -echoprt
		-echoctl -echoke
	/];
}

#Override *_delim to set the linebreak of the device.
sub _build_write_delim { "\n"   }
sub _build_read_delim  { "\r\n" }

sub _build_modes {{
	"Channel" => {
		"Type"          => "selection",
		"Channel A"     => "MA",
		"Channel B"     => "MB",
		"Ch.A - Ch.B"   => "MC",
		"Both Channels" => "MD",
		"No Channels"   => "M@",
	},
	
	"Unit" => {
		"Type"   => "selection",
		"Deg. C" => "U0",
		"Deg. K" => "U1",
		"Deg. F" => "U2",
		"Ohms"   => "U3"
	},
	
	"High Res" => {
		"Type" => "toggle",
		"Off"  => "R0",
		"On"   => "R1",
	},
	
	"Zero" => {
		"Type" => "toggle",
		"Off"  => "Z",
		"On"   => "Z",
	},
	
	"Lock" => {
		"Type" => "toggle",
		"Off"  => "L0",
		"On"   => "L1",
	},
	
	"Restart" => {
		"Type"    => "button",
		"Pressed" => "C",
	},
}}

sub _build_commands {
	{}
}

my %unitconv = (
	'W' => 'Ohms',
	'C' => 'DegC',
	'F' => 'DegF',
	'K' => 'DegK',
);

my %errconv = (
	'E1'  => 'Balance error / No PRT, PRT open circuit / Ratio over range.',
	'E2'  => 'Temperature over range / PRT at temperature outside limits of conversion table.',
	'E3'  => 'No "A" ROM calibration / No ROM calibration on selected Input or Channel',
	'E4'  => 'IEEE or RS232 error / Unrecognized instruction sent',
	'E5'  => 'IEEE or RS232 error / Illegal argument sent',
	'E6'  => 'RAM failure',
	'E7'  => 'Data validation error in CAL ROM “A” or “B” - PRT calibration invalid',
	'E8'  => 'Unable to track temperature change / temperature change too large',
	'E9'  => 'Conversion table too big (more than 395 points) / Resistance range too great',
	'E10' => 'Unable to create resistance/temperature conversion table.',
	'E11' => 'Unable to create resistance/temperature conversion table.',
);

sub _build_thread {
	my $self = shift;
	#Just looking at this construct scares me..
	my $rouse = \sub{};
	$self->thread_rouse($rouse);
	
	return async {
		$self->handle->on_error(sub {$$rouse->(undef, @_)});
		$self->handle->on_eof(sub {$$rouse->(undef, @_)});
		my $lastwords;
		while() {
			$$rouse = Coro::rouse_cb;
			$self->read($$rouse);
			my ($succ, $line, $ex) = Coro::rouse_wait($$rouse);
			unless ($succ) {
				$lastwords = $ex;
				last;
			}
			
			#Remove the spaces.
			$line =~ s/\s+//g;
			if ($line =~ /^(?<channel>[ABD])(?<value>[\+\-\d\.]+)(?<units>[CFKRW])/){
				say "got $+{value}, $unitconv{$+{units}}";
				
				$self->outpipe->put({
					data => {
						device => __PACKAGE__,
						value  => $+{value},
						units  => $unitconv{$+{units}},
						id     => 'UPDATE',
						time   => time(),
					}
				});
			}
			
			elsif ($line =~ /^(?<error>E\-?\d+)/) {
				$self->outpipe->put({
					data => {
						device => __PACKAGE__,
						error  => $errconv{$+{error}},
						id     => 'UPDATE',
						time   => time(),
					}
				});
			}
			else {
				say "Failed to parse temperature! Value was: $line";
			}
		}
		#Turns out if we don't destroy the handle properly, a whole lot of bad shit happens! 
		#AND AGAIN I spend like an hour debugging to figure it out!
		$self->unload;
		$self->handle->push_shutdown;
		
		$self->outpipe->put({
			error =>{
				device => __PACKAGE__,
				error  => $lastwords,
			}
		});
	};
}

sub set_mode {
	my ($self, $mode, $state) = @_;

	#Convert the mode, set the mode. Easy.
	my $str = $self->modes->{$mode}->{$state};
	$self->write($str);
}

__PACKAGE__->meta->make_immutable;
