use 5.010;

package Device::FlukeBath;
use Mouse;
extends 'Device';
with 'Interface::COM';

sub _build_stty_params {
	return [qw/
		2400 cs8 -parenb -cstopb cread raw 
		-opost -olcuc -ocrnl -onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
		-isig -icanon -iexten -echo -echoe -echok -echonl -noflsh -xcase -tostop -echoprt
		-echoctl -echoke
	/];
}

sub _build_modes {{
	"Set Point" => {
		"Type" => "input",
		"set"  => "s=%s",
	},
	
	"Read Rate" => {
		"Type" => "input",
		"set"  => "sa=%d",
	},
	
	"Unit" => {
		"Type"   => "selection",
		"Deg. C" => "u=c",
		"Deg. F" => "u=f",
	},
}}

sub _build_queries {{
	"Set point"   => "s",
	"Temperature" => "t",
}}


#Override *_delim to set the linebreak of the device.
sub _build_write_delim { "\r"   }
sub _build_read_delim  { "\n\n" }



__PACKAGE__->meta->make_immutable;
