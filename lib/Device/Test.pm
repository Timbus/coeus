use 5.010;

package Device::Test;
use Mouse;
use Coro;
use AnyEvent::Handle;

extends 'Device';

sub _build_modes {
	{}
}

sub _build_commands {
	{}
}

sub connect {
	my $fh = IO::File->new('/dev/null')
		or die "Can't open /dev/null: $!";
	my $ufh = AnyEvent::Handle->new(fh => $fh)
		or die "Can't create async handle: $!";
	return $ufh;
}

sub _build_thread {
	my $self = shift;
	my $quit = 0;
	my $rouse = \sub{$quit = 1};
	$self->thread_rouse($rouse);
	
	return async {
		while(!$quit) {
			$self->outpipe->put({
				data => {
					device => __PACKAGE__,
					value  => sprintf('%0.2f', rand(20)),
					units  => 'DegC',
					id     => 'UPDATE',
					time   => time(),
				}
			});
			
			Coro::AnyEvent::sleep(1);
		}
	};
}

sub set_mode {

}

__PACKAGE__->meta->make_immutable;
