use 5.010;

package Device::Yokogawa7563;
use Mouse;
extends 'Device';
with 'Interface::FakeGPIB';


my %unitconv = (
	'O' => 'Ohms',
	'V' => 'Volts',
	'C' => 'DegC',
	'F' => 'DegF',
	'K' => 'DegK',
);

sub start_read {
	my $self = shift;
	$self->write("++auto 1");
	
	my @handler; @handler = ('line' => $self->read_delim,  sub {
		my ($fh, $line) = @_;
		#ALWAYS discard the first line that comes from this device. The gpib is buggy or something.
		state $firstrun = return $fh->push_read(@handler);
		my $time = time;
		my $match = $line =~ /^
			(?<header>
				(?<status> [A-Z] )
				(?<type>
					(?<maintype> [A-Z] )
					(?<subtype> [A-Z] )
				)
				(?<unit> [A-Z] )
				\s?
			)
			(?<value> [\+\-]? [\.\d]+ E[\+\-]\d+ )
		$/x;
		
		Device::Error::ParseError->throw("Failed to parse temperature! Value was: $line") 
			unless $match;
		my %parsed = %+;
	
		if ($parsed{status} !~ /[NSHLP]/) {
			#Error or overrange or whatever. Add exception code later.
			return (undef, 'Blank');
		}
	
		$self->handle->push_read(@handler);
		
		##TODO: Add the value to a list, or an in-memory database.
		##Also think of how to signal a push event (a condvar maybe?)
		
		#return ($parsed{value}+0, $unitconv{$parsed{unit}});
	});
	$self->handle->push_read(@handler);
}

sub stop_read {
	my $self = shift;
	handle->on_read(undef);
}

sub getval {
	my $self = shift;
	
}



__PACKAGE__->meta->make_immutable;
