use 5.010;
package Interface::COM;
use Mouse::Role;

use IO::File;
use AnyEvent::Handle;

has 'stty_params' => (
	is      => 'ro',
	isa     => 'ArrayRef',
	builder => '_build_stty_params',
);

sub _build_stty_params {
	return [qw/ 9600 cs8 -parenb -cstopb cread raw/]
	           #baud  8  -  N   -  1     readable
}


sub connect {
	my $self = shift;
	my $status = system('stty', '-F', $self->interface, @{$self->stty_params});
	die "Failed to set TTY parameters" if (($status >>=8) != 0);
	my $fh = IO::File->new($self->interface, O_RDWR)
		or die "Can't open ".$self->interface.": $!";
	my $ufh = AnyEvent::Handle->new(fh => $fh)
		or die "Can't create async stty handle: $!";
	return $ufh;
}

1;
