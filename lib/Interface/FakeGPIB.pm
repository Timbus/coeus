use 5.010;

package Interface::FakeGPIB;
use Mouse::Role;

use IO::File;
use Coro::Handle;

sub connect {
	my $self = shift;
	my $fh = IO::File->new($self->interface, O_RDWR)
		or die "Can't open ".$self->interface.": $!";
	my $ufh = AnyEvent::Handle->new(fh => $fh, timeout => 10, on_timeout => undef)
		or die "Can't create async gpib handle: $!";
	return $ufh;
}

1;
