use 5.010;

package ModHandler;
use Mouse;
use AnyEvent;
use AnyEvent::Filesys::Notify;
use File::Spec;
use Module::Load;
use YAML::XS qw(LoadFile DumpFile);

has 'outpipe' => (
	is       => 'ro',
	required => 1,
);

has 'conf_dir' => (
	is       => 'ro',
	required => 1,
);

has 'device_dir' => (
	is       => 'ro',
	required => 1,
);

has 'watcher' => (
	is       => 'ro',
	builder  => '_build_watcher',
);

has 'modules' => (
	is       => 'ro',
	isa      => 'HashRef',
	default  => sub { {} },
);

sub _build_watcher {
	my $self = shift;
	return AnyEvent::Filesys::Notify->new(
		dirs   => [ $self->conf_dir ],
		filter => sub { shift =~ /\.(ya?ml)$/ },
		cb     => sub {
			my (@events) = @_;
			for my $event (@events){
				next if $event->is_dir();
				$self->load_module($event->path) if $event->is_modified;
			}
		},
	);
}

sub BUILD {
	my $self = shift;

	my $mod_glob = File::Spec->catfile($self->device_dir, '*.pm');
	for my $modfile (glob $mod_glob) {
		load $modfile;
	}
	
	my $yml_glob = File::Spec->catfile($self->conf_dir, '*.yml');
	for my $yamlfile (glob $yml_glob){
		$self->load_module($yamlfile);
	}
}

sub load_module {
	my $self = shift;
	my $file = shift;
	my $conf = LoadFile($file)
		or die "Cannot load $file, ", $!;
	
	my $mod_id = $conf->{id};

	#Unload the module (if needed)
	$self->unload_module($mod_id) if (defined $self->modules->{$mod_id});

	#Leave if nothing is to be loaded
	return unless $conf->{enabled};
	
	say "Loading module for '$mod_id'";
	
	my $modulename = $conf->{module};
	my $instance = $modulename->new(
		interface => $conf->{interface},
		outpipe   => $self->outpipe,
		init_cmds => $conf->{init} // {},
		exit_cmds => $conf->{exit} // {},
	);
	$self->modules->{$mod_id} = $instance;

}

sub unload_module {
	my $self   = shift;
	my $mod_id = shift;

	say "Unloading module for '$mod_id'";
	
	my $dead_mod = delete $self->modules->{$mod_id};
	$dead_mod->send_input(undef, undef, "Module unloaded");

	#Force the module thread to end. Otherwise the dying object will mess everything up.
	$dead_mod->thread->cede_to;
}

#Safe cleanup, call from signal handlers
sub DEMOLISH {
	my $self = shift;
	$self->unload_module($_) for keys %{$self->modules};
}


__PACKAGE__->meta->make_immutable();
