use 5.010;

package Device;
use Mouse;
use AnyEvent;

has 'units' => (
	is       => 'ro',
	isa      => 'ArrayRef',
	init_arg => undef,
	default  => sub { [qw/
		DegC
		DegF
		DegK
		Ohms
		Volts
		Amps
		RH
	/] },
);

has 'commands' => (
	is      => 'rw',
	isa     => 'HashRef',
	builder => '_build_commands',
);

has 'init_cmds' => (
	is      => 'ro',
	isa     => 'HashRef',
);

has 'exit_cmds' => (
	is      => 'ro',
	isa     => 'HashRef',
);

#This is the physical interface address, ie '/dev/ttyS0'
has 'interface' => (
	is       => 'ro',
	required => 1,
);
#This is the async handle attached to the above interface. 
has 'handle' => (
	is      => 'ro',
	lazy    => 1,
	builder => 'connect', #Inherited from the apropos connection role
);

#This is the 'thread' that handles all of the device reading.
has 'thread' => (
	is      => 'ro',
	lazy    => 1,
	builder => '_build_thread',
);
has 'thread_rouse' => (
	is => 'rw',
);

sub send_input {
	my $self = shift;
	${$self->thread_rouse}->(@_);
}

#Finally, this is the coro::channel that will pass results back to the mainloop after being parsed from the handle.
has 'outpipe' => (
	is       => 'ro',
	required => 1,
);

has 'write_delim' => (
	is      => 'ro',
	isa     => 'Str',
	builder => '_build_write_delim',
);
has 'read_delim' => (
	is      => 'ro',
	isa     => 'Str',
	builder => '_build_read_delim',
);

has 'modes' => (
	is      => 'ro',
	builder => '_build_modes',
);

sub _build_read_delim {
	"\r\n"
}
sub _build_write_delim {
	"\r\n"
}

sub write {
	my $self = shift;
	say "Writing @_ to device";
	$self->handle->push_write("@_" . $self->write_delim);
}

sub read {
	my $self = shift;
	my $cb = shift;
	$self->handle->push_read('line' => $self->read_delim, $cb);
}

sub BUILD {
	my $self = shift;
	while (my ($setting, $state) = each %{$self->init_cmds}){
		$self->set_mode($setting, $state);
	}
	$self->thread;
}

sub unload {
	my $self = shift;
	while (my ($setting, $state) = each %{$self->exit_cmds}){
		$self->set_mode($setting, $state);
	}
}


__PACKAGE__->meta->make_immutable;
