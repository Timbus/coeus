#!/usr/bin/perl
use 5.010;
use strict;
use warnings;
use autodie;

use File::Basename;
use File::Spec;

use lib File::Spec->catdir(dirname(__FILE__), 'lib');

use EV;
use AnyEvent;
use AnyEvent::Socket;
use AnyEvent::Handle;
use AnyEvent::Filesys::Notify;
use JSON::XS;
use Coro;

use Scalar::Util 'refaddr';
use Try::Tiny;
use ModHandler;

my $unloop = AE::cv;
my $outpipe = Coro::Channel->new();
my %clients;

my $mods = ModHandler->new(
	conf_dir   => File::Spec->rel2abs(File::Spec->catdir(dirname(__FILE__), 'conf')),
	device_dir => File::Spec->rel2abs(File::Spec->catdir(dirname(__FILE__), 'lib', 'Device')), 
	outpipe    => $outpipe
);

#The tcp server merely handles incoming connections.
sub handle_connection {
	say "Got a new connection";
	#Make a handle for the new user.
	my ($rawfh, $host, $port) = @_;
	my $fh; $fh = AnyEvent::Handle->new(
		fh => $rawfh,
		on_error => sub { delete $clients{refaddr($fh)} },
	);
	#Make a thread to handle all input from the client handle.
	my $client_thread = async (\&handle_input, $fh);
	
	#Stash the client info so we can use it whenever, 
	#use $fh as a key so we can find it pretty easily.
	$clients{refaddr($fh)} = {
		host   => $host,
		thread => $client_thread,
		fh     => $fh,
	};
}
#Set up the server on port 9999 (use a config file later to change this)
tcp_server (undef, 9999, \&handle_connection);


#Here we handle input. We do not pass anything back to the client unless there 
#is a failure in the handler itself.
sub handle_input {
	my $fh = shift;
	my $rouse;

	$fh->on_error(sub { say $_[2]; $$rouse->(undef) });
	while () {
		$$rouse = Coro::rouse_cb;
		$fh->push_read('json' => $$rouse);
		my ($succ, $rpc) = Coro::rouse_wait;
		last unless (defined $succ);

		my $method = $rpc->{method};

		#Special case (hate doing this..)
		last if $method eq 'CLOSE';

		#Entering bat country:
		no strict 'refs';
		try {
			$method->($fh, $rpc);
		}
		catch {
			my $ret = {
				error   => $_,
				jsonrpc => '2.0',
				id      => $rpc->{id},
			};
			$fh->push_write(json => $ret);
		};
		use strict 'refs';
	}
	
	say 'Disconnecting ', $clients{refaddr($fh)}{host};
	#Clean up the threads and handles and junk.
	delete $clients{refaddr($fh)};
}


#Here is a single thread to return data back to the clients.
#I guess the values will need to all conform to a spec or something.
#For now we'll just push the return vals..
async {
	while (defined(my $ret = $outpipe->get())) {
		if ($ret->{data}){
			my $json;
			$json->{result} = $ret->{data};
			$json->{jsonrpc} = '2.0';
			for my $client (values %clients){
				$client->{fh}->push_write(json => $json);
			}
		}
		elsif ($ret->{error}){
			
		}
	}
};

#Need to store these callbacks so they don't get cleaned up. Perl trying to be too smart.
my $traps = [
	AE::signal("INT"  => sub { $mods->DESTROY; $unloop->send() }),
	AE::signal("TERM" => sub { $mods->DESTROY; $unloop->send() }),
	AE::signal("HUP"  => sub { $mods->DESTROY; $unloop->send() }),
	AE::signal("PIPE" => sub { $mods->DESTROY; $unloop->send() }),
];

$unloop->recv();

