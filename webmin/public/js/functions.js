"use strict";

//Get a JSON websocket going (? maybe?)



//Hook up typeahead

$('[data-typeahead-url]').each(function(){
	$(this).typeahead({
		ajax: {
			url: $(this).data('typeahead-url'),
			triggerLength: 1
		}
	});
});

//Hook up buttons
$('#start-coeus').click(function(){
	if ($(this).hasClass('disabled'))
		return;

	$(this).button('loading');

	var request = $.ajax({
		url: '/ajax/start_coeus/',
		method: 'POST'
	});

	request.always(function(){
		$('#start-coeus').button('reset');
	});

	request.done(function(ret){
		if (ret == 'success'){
			//Use a timeout so .button(reset) doesn't ruin the css.
			setTimeout(function() {
				$('#stop-coeus').removeClass('disabled');
				$('#start-coeus').addClass('disabled');
				$('.server-state').text('running.'); 
			});
		}
		else {
			alert( "Could not start Coeus: " + ret );
		}

	});
	request.fail(function(jqXHR, textStatus) {
 		alert( "Request failed: " + textStatus );
	});
});

$('#stop-coeus').click(function(){
	if ($(this).hasClass('disabled'))
		return;

	$(this).button('loading');

	var request = $.ajax({
		url: '/ajax/stop_coeus/',
		method: 'POST'
	});

	request.always(function(){
		$('#stop-coeus').button('reset');
	});

	request.done(function(ret){
		if (ret == 'success'){
			//Use a timeout so .button(reset) doesn't ruin the css.
			setTimeout(function() {
				$('#start-coeus').removeClass('disabled');
				$('#stop-coeus').addClass('disabled');
				$('.server-state').text('stopped.'); 
			});
		}
		else {
			alert( "Could not stop Coeus: " + ret );
		}

	});
	request.fail(function(jqXHR, textStatus) {
 		alert( "Request failed: " + textStatus );
	});
});

$('.apply-changes').click(function(){
	var btn = $(this);
	var device = btn.attr("id");

	var data = {};
	data.enabled  = $('#' + device + "-enabled-button").hasClass('active') ? '1' : '';
	data.module   = $('#' + device + "-module-input").val();
	data.interface= $('#' + device + "-interface-input").val();
	data.filename = $('#' + device + "-filename-hidden").val();

	var request = $.ajax({
		url: '/ajax/save_config/',
		data: data
	});

	btn.button('loading');

	request.done(function(){
		if (data.enabled) {
			$('#' + device + '-container').addClass("alert-success");
		}
		else {
			$('#' + device + '-container').removeClass("alert-success");
		}
		btn.button('success');
		setTimeout(function(){
			btn.button('reset');
		}, 2000);
	});
});
