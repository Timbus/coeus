package Webmin::Root;
use Mojo::Base 'Mojolicious::Controller';

use YAML::XS qw/LoadFile DumpFile/;
use File::Spec;
use File::Glob 'bsd_glob';
use Daemon;

sub index {
	my $self = shift;
	
	state $mod_conf_dir = File::Spec->catfile($self->config->{coeus_path}, 'conf');
	state $last_modified = 0;
	state $conf_files = {};
	state $conf_contents = {};

	my $pattern = File::Spec->catfile($mod_conf_dir, '*.{yml,yaml,conf}');
	for my $filename ( bsd_glob($pattern) ){
		#Get the files' "last modified" time.
		my $modified = (stat($filename))[9];
		
		#Load the yaml file if needed.
		if (!$conf_files->{$filename} || $conf_files->{$filename} != $modified){
			$conf_files->{$filename} = $modified;
			my $yml = LoadFile($filename);
			$conf_contents->{$filename} = $yml;
		}
	}

	#Get the status of the server.
	my $pidfile = File::Spec->catfile($self->config->{coeus_path}, $self->config->{pid_file});
	my $running = -e $pidfile && Proc::Daemon->Status($pidfile);

	$self->render(
		confs   => $conf_contents,
		running => $running,
	);

}

1;
