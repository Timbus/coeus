package Webmin::Ajax;
use Mojo::Base 'Mojolicious::Controller';

use File::Spec;
use Daemon;
use Try::Tiny;
use YAML::XS qw/LoadFile DumpFile/;
use File::Glob 'bsd_glob';

sub start_coeus {
	my $self = shift;

	try {
		my $pid = Proc::Daemon::Init({
			work_dir     => $self->config->{coeus_path},
			pid_file     => $self->config->{pid_file},
			exec_command => File::Spec->catfile($self->config->{coeus_path}, 'Coeus.pl'),
		});

		if ($pid){
			$self->render(
				text => 'success',
			);
		}
		else {
			$self->render(
				text => 'No PID. He\'s dead Jim',
			);
		}
	}
	catch {
		$self->render(
			text => $!,
		);
	}
}

sub stop_coeus {
	my $self = shift;
	say 'stopping';
	my $pidfile = File::Spec->catfile($self->config->{coeus_path}, $self->config->{pid_file});
	my $success = Proc::Daemon->Kill_Daemon($pidfile);
	$self->render(
		text => $success ? 'success' : 'Could not find pid.'
	);
}

sub save_config {
	my $self = shift;
	
	try {
		my $filename = $self->param('filename');
		my $yml = LoadFile($filename);
		for my $name ($self->param) {
			next if $name eq 'filename';
			$yml->{$name} = $self->param($name);
		}
		DumpFile($filename, $yml);
	
		$self->render(
			text => 'success'
		);
	}
	catch {
		$self->render(
			text => $@
		);
	}
}

sub get_modules {
	my $self = shift;

	my $mod_glob = File::Spec->catfile($self->config->{coeus_path}, 'lib', 'Device', '*.pm');
	my $mods = [map {/Device\/([^.]+)\.pm$/; "Device::$1"} bsd_glob($mod_glob)];

	$self->render(
		json => $mods
	)
}

sub get_interfaces {
	my $self = shift;

	my $usb_glob = File::Spec->catfile('/', 'dev', 'ttyUSB*');
	my $pts_glob = File::Spec->catfile('/', 'dev', 'pts', '*');
	my $ifaces = [bsd_glob($usb_glob), bsd_glob($pts_glob)];

	$self->render(
		json => $ifaces
	)
}

1;
