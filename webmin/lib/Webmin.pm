package Webmin;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
	my $self = shift;
	
	$self->plugin('JSONConfig', {
		file => 'webmin.conf.json',
	});
	

	# Router
	my $r = $self->routes;

	# Normal route to controller
	$r->route('/')->to('root#index');

	$r->route('/ajax/:action')->to(controller => 'ajax');
}

1;
